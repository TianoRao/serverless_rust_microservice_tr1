use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use statrs::distribution::{ContinuousCDF, Normal};
use std::f64::consts::E;

// Adjusted data model for DynamoDB
#[derive(Debug, Serialize, Deserialize)]
struct OptionResult {
    result_id: String,
    call_price: f64,
}

// Request structure for Lambda event
#[derive(Deserialize)]
struct Request {
    s0: f64,
    x: f64,
    r: f64,
    sigma: f64,
    t: f64,
}

// Response structure for Lambda event
#[derive(Serialize)]
struct Response {
    message: String,
    call_price: f64,
}

fn black_scholes_call_price(s0: f64, x: f64, r: f64, sigma: f64, t: f64) -> f64 {
    let d1 = (s0.ln() / x + (r + sigma.powi(2) / 2.0) * t) / (sigma * t.sqrt());
    let d2 = d1 - sigma * t.sqrt();

    let norm_dist = Normal::new(0.0, 1.0).unwrap();
    let nd1 = norm_dist.cdf(d1);
    let nd2 = norm_dist.cdf(d2);

    s0 * nd1 - x * E.powf(-r * t) * nd2
}

// Adapted Lambda function handler
async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let params = event.payload;
    let call_price = black_scholes_call_price(params.s0, params.x, params.r, params.sigma, params.t);

    let config = aws_config::load_from_env().await;
    let client = Client::new(&config);

    // Assuming a DynamoDB table structure and storing the calculation result
    let result_id = uuid::Uuid::new_v4().to_string(); // Generate unique ID for the entry
    let _ = client.put_item()
        .table_name("OptionResults")
        .item("result_id", AttributeValue::S(result_id.clone()))
        .item("call_price", AttributeValue::N(call_price.to_string()))
        .send()
        .await
        .map_err(|err| err.to_string())?;

    Ok(Response {
        message: format!("Successfully calculated call option price."),
        call_price,
    })
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    run(service_fn(function_handler)).await
}
