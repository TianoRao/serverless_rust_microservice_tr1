rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version 			#rust compiler
	cargo --version 			#rust package manager
	rustfmt --version			#rust code formatter
	rustup --version			#rust toolchain manager
	clippy-driver --version		#rust linter

format:
	cargo fmt --quiet

install:
	# Install if needed
	#@echo "Updating rust toolchain"
	#rustup update stable
	#rustup default stable 

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

watch:
	cargo lambda watch

build: 
	cargo lambda build --release 

deploy:
	cargo lambda deploy 

aws-invoke: 
	cargo lambda invoke --remote mini5_tr --data-ascii "{\"s0\": 100.0, \"x\": 95.0, \"r\": 0.05, \"sigma\": 0.2, \"t\": 1.0}"

invoke:
	cargo lambda invoke --data-ascii "{\"s0\": 100.0, \"x\": 95.0, \"r\": 0.05, \"sigma\": 0.2, \"t\": 1.0}"

all: format lint test 