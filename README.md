# Serverless_Rust_Microservice
By Tianji Rao

## Introduction
In this project we build a serverless rust microservice which can calculate Black-Scholes option price and save the result to a Dynamol Table on AWS. This Rust project calculates the call option price using the Black-Scholes formula and stores the result in an AWS DynamoDB table. It's designed to run as an AWS Lambda function, providing a serverless solution to perform financial computations and data storage.


## Project Structure

- `black_scholes.rs` - Contains the core logic for the Black-Scholes call price calculation.
- `handler.rs` - Defines the AWS Lambda function handler, including interaction with DynamoDB.
- `Cargo.toml` - Manages Rust project dependencies.

## Dependencies

- `aws-sdk-dynamodb` for DynamoDB client operations.
- `lambda_runtime` for creating Lambda functions in Rust.
- `serde`, `serde_derive` for serializing and deserializing JSON data.
- `statrs` for statistical functions, specifically for the normal distribution.
- `uuid` for generating unique identifiers for database entries.
- `tokio` as the asynchronous runtime.

## Setup

1. **Rust Environment:** Ensure you have Rust and Cargo installed.
2. **AWS CLI:** Configure AWS CLI with appropriate credentials.
3. **DynamoDB Table:** Create a DynamoDB table named `OptionResults` with `result_id` as the primary key.

## Deployment

1. Compile the project for the AWS Lambda environment using a suitable target, for example, `x86_64-unknown-linux-musl`.
2. Package the binary and dependencies as a zip file.
3. Deploy the zip file to AWS Lambda via the AWS CLI or AWS Management Console.
4. Set the handler to the compiled binary name.

## Usage

The Lambda function expects an event with the following JSON structure:

```json
{
  "s0": <current stock price>,
  "x": <strike price>,
  "r": <risk-free interest rate>,
  "sigma": <volatility>,
  "t": <time to maturity in years>
}

```

## Example
![Screenshot_2024-02-29_at_8.02.04_PM](/uploads/c6d7c17a1daa91fbcfa1dd08f96420cb/Screenshot_2024-02-29_at_8.02.04_PM.png)

![Screenshot_2024-02-29_at_8.02.20_PM](/uploads/4b45cacd0092887466ebe1eea015d6c8/Screenshot_2024-02-29_at_8.02.20_PM.png)